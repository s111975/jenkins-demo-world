#!/bin/bash
set -e
mvn package -Dquarkus.package.type=uber-jar

docker-compose up -d --build

docker image prune -f

sleep 2s

cd ../hello-world
mvn test